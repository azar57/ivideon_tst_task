////
//// server.cpp
//// ~~~~~~~~~~
////
//// Copyright (c) 2003-2013 Christopher M. Kohlhoff (chris at kohlhoff dot com)
////
//// Distributed under the Boost Software License, Version 1.0. (See accompanying
//// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
////

//#include <boost/asio.hpp>
//#include <boost/date_time/posix_time/posix_time.hpp>
//#include <ctime>
//#include <iostream>
//#include <string>

//using boost::asio::ip::tcp;

//std::string make_daytime_string()
//{
//    std::cout << "sending respose!\n";
//    using namespace std; // For time_t, time and ctime;

//    time_t now = time(0);
//    return ctime(&now);
//}

//int main()
//{
//    try {
//        boost::asio::io_service io_service;

//        tcp::acceptor acceptor(io_service, tcp::endpoint(tcp::v4(), 1333));

//        for (;;) {
//            tcp::socket socket(io_service);
//            acceptor.accept(socket);
//            std::cout << "got a request!\n";

//            boost::asio::deadline_timer t(io_service, boost::posix_time::seconds(10));
//            t.wait();
//            std::string message = make_daytime_string();

//            boost::system::error_code ignored_error;
//            boost::asio::write(socket, boost::asio::buffer(message), ignored_error);
//        }
//    }
//    catch (std::exception &e) {
//        std::cerr << e.what() << std::endl;
//    }

//    return 0;
//}


//
// server.cpp
// ~~~~~~~~~~
//
// Copyright (c) 2003-2020 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>
#include <ctime>
#include <iostream>
#include <string>
#include <unordered_map>

#include "ledCommon.h"


using boost::asio::ip::tcp;
using boost::system::error_code;
using boost::system::errc::errc_t;
using boost::system::errc::make_error_code;


using std::cout;
using std::endl;
using std::string;
using std::tuple;
using std::unordered_map;


const char *state_ok = "OK";
const char *state_fail = "FAILED";

const char *comand_set_led_state = "set-led-state";
const char *comand_get_led_state = "get-led-state";
const char *comand_set_led_color = "set-led-color";
const char *comand_get_led_color = "get-led-color";
const char *comand_set_led_rate = "set-led-rate";
const char *comand_get_led_rate = "get-led-rate";

const char *on = "on";
const char *off = "off";

const char *color_red = "red";
const char *color_green = "green";
const char *color_blue = "blue";

//enum class CommandResult { Ok, Fail}; //не нужно, use error_code


class LedMock : public ILedCommander
{
    LedState state = LedState::None;
    LedColor color = LedColor::None;
    size_t rate = 0;
    // ILedCommander interface
public:
    CommandResult setLedState(LedState new_state) override
    {
        state = new_state;
        cout << LedHelper::ledStateStringRepresentation.at(state) << endl;
        return CommandResult::Ok;
    }

    tuple<CommandResult, LedState> getLedState() override
    {
        if (state == LedState::None)
            return { { CommandResult::Failed }, state };
        return { CommandResult::Ok, state };
    }

    CommandResult setLedColor(LedColor new_color) override
    {
        color = new_color;
        cout << LedHelper::ledColorStringRepresentation.at(color) << endl;
        return CommandResult::Ok;
    }

    tuple<CommandResult, LedColor> getLedColor() override
    {
        if (color == LedColor::None)
            return { { CommandResult::Failed }, color };
        return { CommandResult::Ok, color };
    }

    CommandResult setLedRate(size_t new_rate) override
    {
        rate = new_rate;
        cout << "Rate " << rate << endl;
        return CommandResult::Ok;
    }
    tuple<CommandResult, size_t> getLedRate() override
    {
        if (1 > rate || rate > 5)
            return { { CommandResult::Failed }, rate };
        return { CommandResult::Ok, rate };
    }
};


std::string make_daytime_string2(boost::asio::io_context &io_context)
{
    //    boost::asio::deadline_timer t(io_context, boost::posix_time::seconds(10));

    using namespace std; // For time_t, time and ctime;
    cout << "creating response!"
         << endl;

    //    t.wait();
    cout << "created!"
         << endl;

    time_t now = time(0);
    return ctime(&now);
}

class tcp_connection
    : public boost::enable_shared_from_this<tcp_connection>
{
public:
    typedef boost::shared_ptr<tcp_connection> pointer;

    static pointer create(boost::asio::io_context &io_context)
    {
        return pointer(new tcp_connection(io_context));
    }

    tcp::socket &socket()
    {
        return socket_;
    }

    void start()
    {
        message_ = make_daytime_string2(io_context_);
        //        timer.async_wait([this](auto) {
        //            boost::asio::async_write(socket_, boost::asio::buffer(message_), boost::bind(&tcp_connection::handle_write, shared_from_this(), boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
        //        });

        boost::asio::async_write(socket_, boost::asio::buffer(message_), boost::bind(&tcp_connection::handle_write, shared_from_this(), boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
    }

private:
    tcp_connection(boost::asio::io_context &io_context)
        : socket_(io_context),
          timer(io_context, boost::posix_time::seconds(10)),
          io_context_(io_context)
    {
    }

    void handle_write(const boost::system::error_code & /*error*/,
                      size_t /*bytes_transferred*/)
    {
    }

    tcp::socket socket_;
    std::string message_;
    boost::asio::deadline_timer timer;
    boost::asio::io_context &io_context_;
};

class tcp_server
{
public:
    tcp_server(boost::asio::io_context &io_context)
        : io_context_(io_context),
          acceptor_(io_context, tcp::endpoint(tcp::v4(), 1333))
    {
        start_accept();
    }

private:
    void start_accept()
    {
        tcp_connection::pointer new_connection =
            tcp_connection::create(io_context_);

        acceptor_.async_accept(new_connection->socket(),
                               boost::bind(&tcp_server::handle_accept, this, new_connection, boost::asio::placeholders::error));
    }

    void handle_accept(tcp_connection::pointer new_connection,
                       const boost::system::error_code &error)
    {
        if (!error) {
            new_connection->start();
        }

        start_accept();
    }

    boost::asio::io_context &io_context_;
    tcp::acceptor acceptor_;
};

int main()
{
    try {
        boost::asio::io_context io_context;
        tcp_server server(io_context);
        io_context.run();
    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}
