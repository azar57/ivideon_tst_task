#ifndef LEDCOMMON_H
#define LEDCOMMON_H

#include <string>
#include <unordered_map>

using std::string;
using std::unordered_map;


enum class LedColor { None = 0,
                      Red,
                      Green,
                      Blue };
enum class LedState { None = 0,
                      On,
                      Off };

enum class CommandResult { Ok,
                           Failed };

struct LedHelper
{
    static const unordered_map<LedState, string> ledStateStringRepresentation;
    static const unordered_map<string, LedState> ledStateReversStringRepresentation;
    static const unordered_map<LedColor, string> ledColorStringRepresentation;
    static const unordered_map<string, LedColor> ledColorReverseStringRepresentation;
    static const unordered_map<CommandResult, string> commandResultStringRepresentation;

    static unordered_map<LedState, string> createLedStateStringRepresentation();
    static unordered_map<string, LedState> createLedStateReversStringRepresentation();
    static unordered_map<LedColor, string> createLedColorStringRepresentation();
    static unordered_map<string, LedColor> createLedColorReverseStringRepresentation();
    static unordered_map<CommandResult, string> createCommandResultStringRepresentation();
};


#endif //LEDCOMMON_H
