#ifndef CONSOLEUI_H
#define CONSOLEUI_H

#include <functional>
#include <string>
#include <vector>

using std::function;
using std::string;
using std::vector;

class ConsoleUI
{
public:
    ConsoleUI();
    void exec();

private:
    struct Action
    {
        string description;
        function<void(void)> fn;
    };

    vector<Action> actions;
    bool looping = true;
};

#endif //CONSOLEUI_H
