#ifndef ILEDCOMMANDER_H
#define ILEDCOMMANDER_H

#include <tuple>

#include "ledCommon.h"

using std::tuple;

class ILedCommander
{
public:
    //todo: add docs there
    virtual CommandResult setLedState(LedState new_state) = 0;
    virtual tuple<CommandResult, LedState> getLedState() = 0;
    virtual CommandResult setLedColor(LedColor color) = 0;
    virtual tuple<CommandResult, LedColor> getLedColor() = 0;
    virtual CommandResult setLedRate(size_t rate) = 0;
    virtual tuple<CommandResult, size_t> getLedRate() = 0;
};

#endif //ILEDCOMMANDER_H
