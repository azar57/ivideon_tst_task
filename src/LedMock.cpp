#include "LedMock.h"

#include <iostream>

using std::cout;
using std::endl;

CommandResult LedMock::setLedState(LedState new_state)
{
    state = new_state;
    cout << LedHelper::ledStateStringRepresentation.at(state) << endl;
    return CommandResult::Ok;
}

tuple<CommandResult, LedState> LedMock::getLedState()
{
    if (state == LedState::None)
        return { { CommandResult::Failed }, state };
    return { CommandResult::Ok, state };
}

CommandResult LedMock::setLedColor(LedColor new_color)
{
    color = new_color;
    cout << LedHelper::ledColorStringRepresentation.at(color) << endl;
    return CommandResult::Ok;
}

tuple<CommandResult, LedColor> LedMock::getLedColor()
{
    if (color == LedColor::None)
        return { { CommandResult::Failed }, color };
    return { CommandResult::Ok, color };
}

CommandResult LedMock::setLedRate(size_t new_rate)
{
    rate = new_rate;
    cout << "Rate " << rate << endl;
    return CommandResult::Ok;
}

tuple<CommandResult, size_t> LedMock::getLedRate()
{
    if (1 > rate || rate > 5)
        return { { CommandResult::Failed }, rate };
    return { CommandResult::Ok, rate };
}
