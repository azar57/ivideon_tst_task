#ifndef LEDMOCK_H
#define LEDMOCK_H

#include "ILedCommander.h"

/**
 * @brief The LedMock class - имиитатор контроллера светодиода камеры
 * описание методов см в интерфейсе @class ILedCommander
 */
class LedMock : public ILedCommander
{
    LedState state = LedState::None;
    LedColor color = LedColor::None;
    size_t rate = 0;
    // ILedCommander interface
public:
    CommandResult setLedState(LedState new_state) override;
    tuple<CommandResult, LedState> getLedState() override;
    CommandResult setLedColor(LedColor new_color) override;
    tuple<CommandResult, LedColor> getLedColor() override;
    CommandResult setLedRate(size_t new_rate) override;
    tuple<CommandResult, size_t> getLedRate() override;
};


#endif //LEDMOCK_H
