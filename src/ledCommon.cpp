#include "ledCommon.h"


const unordered_map<LedState, string> LedHelper::ledStateStringRepresentation = createLedStateStringRepresentation();
const unordered_map<string, LedState> LedHelper::ledStateReversStringRepresentation = createLedStateReversStringRepresentation();
const unordered_map<LedColor, string> LedHelper::ledColorStringRepresentation = createLedColorStringRepresentation();
const unordered_map<string, LedColor> LedHelper::ledColorReverseStringRepresentation = createLedColorReverseStringRepresentation();
const unordered_map<CommandResult, string> LedHelper::commandResultStringRepresentation = createCommandResultStringRepresentation();


unordered_map<LedState, string> LedHelper::createLedStateStringRepresentation()
{
    unordered_map<LedState, string> result;
    result.insert({ { LedState::None, "" }, { LedState::On, "ON" }, { LedState::Off, "OFF" } });
    return result;
}

unordered_map<std::string, LedState> LedHelper::createLedStateReversStringRepresentation()
{
    unordered_map<std::string, LedState> result;
    result.insert({ { "", LedState::None }, { "ON", LedState::On }, { "OFF", LedState::Off } });
    return result;
}

unordered_map<LedColor, std::string> LedHelper::createLedColorStringRepresentation()
{
    unordered_map<LedColor, std::string> result;
    result.insert({ { LedColor::None, "" }, { LedColor::Red, "red" }, { LedColor::Green, "green" }, { LedColor::Blue, "blue" } });
    return result;
}

unordered_map<std::string, LedColor> LedHelper::createLedColorReverseStringRepresentation()
{
    unordered_map<std::string, LedColor> result;
    result.insert({ { "", LedColor::None }, { "red", LedColor::Red }, { "green", LedColor::Green }, { "blue", LedColor::Blue } });
    return result;
}

unordered_map<CommandResult, std::string> LedHelper::createCommandResultStringRepresentation()
{
    unordered_map<CommandResult, std::string> result;
    result.insert({ { CommandResult::Ok, "OK" }, { CommandResult::Failed, "FAILED" } });
    return result;
}
