#include "ConsoleUI.h"

#include <boost/asio.hpp>
#include <iostream>

using std::cin;
using std::cout;
using std::endl;

ConsoleUI::ConsoleUI()
{
    actions.push_back({ "Exit", [this]() { looping = false; } });
    actions.push_back({ "Add new camera", []() {} });
}

void ConsoleUI::exec()
{
    while (looping) {
        cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
        for (int i = 0; i < actions.size(); ++i) {
            cout << i << ". " << actions[i].description << endl;
        }
        cout << "Select option:" << endl;
        int option;
        cin >> option;
        if (option > -1 && option < actions.size()) {
            actions.at(option).fn();
        }
    }
}
